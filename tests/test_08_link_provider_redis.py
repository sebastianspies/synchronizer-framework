from sync.backends.inmemory import InMemoryDs
from demo.fixtures import Company, User
from sync.mixins import LinkProvider


# @pytest.mark.skip(msg="Only works with running redis instance")
def test_link_provider(redis_link_provider: LinkProvider):
    link_provider = redis_link_provider
    c1 = Company(name="some compa:ny")
    u2 = User(name="some :user")

    ds1 = InMemoryDs.from_collection({c1})
    ds2 = InMemoryDs.from_collection({u2})

    link_provider.link(c1, u2, ds1, ds2)

    assert u2 in link_provider.others(c1, ds1, User, ds2)
    assert c1 in link_provider.others(u2, ds2, Company, ds1)
