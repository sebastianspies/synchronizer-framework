import pprint

from demo.daphne import (
    VlanServiceProvider,
)
from demo.desire import CloudServiceProvider
from sync.backends.inmemory import (
    InMemoryDs,
)
from sync.syncer import Syncer


def test_create_single_ltr(conditional_mapping):
    db_a = InMemoryDs.from_collection({VlanServiceProvider(name="AWS"), VlanServiceProvider(name="AZURE")})
    db_b = InMemoryDs.from_collection(set())

    syncer = Syncer(db_a, db_b, conditional_mapping)

    syncer.sync_all()
    assert 1 == len(db_b.all(CloudServiceProvider))
    assert "AWS" == db_b.all(CloudServiceProvider)[0].name


def test_create_single_rtl(conditional_mapping):
    db_a = InMemoryDs.from_collection(set())
    db_b = InMemoryDs.from_collection({CloudServiceProvider(name="AWS"), CloudServiceProvider(name="AZURE")})

    syncer = Syncer(db_a, db_b, conditional_mapping)

    syncer.sync_all()
    assert 1 == len(db_a.all(VlanServiceProvider))
    assert "AZURE" == db_a.all(VlanServiceProvider)[0].name
