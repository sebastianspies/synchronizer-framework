from sync.backends.inmemory import (
    InMemoryDs,
)

from demo.daphne import (
    VlanServiceProvider,
)
from demo.desire import (
    CloudServiceProvider,
)
from sync.mapping import Mode
from sync.operations import Linking, Addition
from sync.syncer import Syncer


def test_create_single_ltr(simple_mapping):
    from_entity = VlanServiceProvider(name="AWS")
    db_a = InMemoryDs.from_collection({from_entity})
    db_b = InMemoryDs.from_collection(set())

    syncer = Syncer(db_a, db_b, simple_mapping)
    operations = syncer.sync_all()

    assert len(operations) == 2
    assert operations[0] == Addition(
        VlanServiceProvider, CloudServiceProvider, from_entity, operations[0].to_entity
    )
    assert operations[1] == Linking(
        frozenset({VlanServiceProvider, CloudServiceProvider}), operations[1].entities
    )
    csp = db_b.all(CloudServiceProvider).pop()
    assert csp.name == "AWS"
    vlsp = db_a.all(VlanServiceProvider).pop()
    assert vlsp.name == "AWS"

    operations = syncer.sync_all()
    assert 0 == len(operations)


def test_create_single_rtl(simple_mapping):
    db_a = InMemoryDs.from_collection(set())
    from_entity = CloudServiceProvider(name="AWS")
    db_b = InMemoryDs.from_collection({from_entity})

    syncer = Syncer(db_a, db_b, simple_mapping)

    operations = syncer.sync_all()
    assert len(operations) == 2
    assert operations[0] == Addition(
        CloudServiceProvider, VlanServiceProvider, from_entity, operations[0].to_entity
    )
    assert operations[1] == Linking(
        frozenset({VlanServiceProvider, CloudServiceProvider}), operations[1].entities
    )

    csp = db_b.all(CloudServiceProvider).pop()
    assert csp.name == "AWS"
    vlsp = db_a.all(VlanServiceProvider).pop()
    assert vlsp.name == "AWS"

    operations = syncer.sync_all()
    assert operations == []


def test_create_bidirectional(simple_mapping):
    db_a = InMemoryDs.from_collection({VlanServiceProvider(name="CSP1")})
    db_b = InMemoryDs.from_collection({CloudServiceProvider(name="CSP2")})
    syncer = Syncer(db_a, db_b, simple_mapping)
    operations = syncer.sync_all()
    assert len(db_b.all(CloudServiceProvider)) == 2
    assert len(db_a.all(VlanServiceProvider)) == 2
    assert len(operations) == 4
    isinstance(operations[0], Addition)
    isinstance(operations[1], Linking)
    isinstance(operations[2], Addition)
    isinstance(operations[3], Linking)
    operations = syncer.sync_all()
    assert operations == []


def test_no_create_unsupported_direction(simple_mapping):
    missing_mapping = simple_mapping.copy()
    missing_mapping[0].modes.remove(Mode.LEFT_TO_RIGHT)

    db_a = InMemoryDs.from_collection({VlanServiceProvider(name="AWS")})
    db_b = InMemoryDs.from_collection(set())

    syncer = Syncer(db_a, db_b, missing_mapping)
    operations = syncer.sync_all()
    assert [] == operations
