from demo.daphne import VlanServiceProvider
from demo.desire import CloudServiceProvider
from sync.backends.inmemory import InMemoryDs
from sync.operations import Addition
from sync.syncer import Syncer


def test_create_single_with_non_key_attributes(simple_mapping):
    csp1_daphne = VlanServiceProvider(name="csp1", canUpgrade=False)

    db_a = InMemoryDs.from_collection({csp1_daphne})
    db_b = InMemoryDs.from_collection({})

    syncer = Syncer(db_a, db_b, simple_mapping)
    operations = syncer.sync_all()
    assert len(operations) == 2

    operations[0] == Addition(
        from_class=VlanServiceProvider,
        to_class=CloudServiceProvider,
        from_entity=csp1_daphne,
        to_entity=operations[0].to_entity,
    )

    assert operations[0].to_entity.upgrade_allowed == csp1_daphne.canUpgrade == False
